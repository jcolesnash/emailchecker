﻿namespace EmailChecker
{
    partial class EmailChecker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHostPop = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPortPop = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPortPop = new System.Windows.Forms.TextBox();
            this.lstActivityPop = new System.Windows.Forms.ListBox();
            this.txtHostPop = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkAuth = new System.Windows.Forms.CheckBox();
            this.txtHostSmtp = new System.Windows.Forms.TextBox();
            this.lblHostSmtp = new System.Windows.Forms.Label();
            this.lblPortSmtp = new System.Windows.Forms.Label();
            this.txtPortSmtp = new System.Windows.Forms.TextBox();
            this.lstActivitySmtp = new System.Windows.Forms.ListBox();
            this.lblActivityPop = new System.Windows.Forms.Label();
            this.lblActivitySmtp = new System.Windows.Forms.Label();
            this.btnEmailError = new System.Windows.Forms.Button();
            this.popWorkerThread = new System.ComponentModel.BackgroundWorker();
            this.smtpWorkerThread = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHostPop
            // 
            this.lblHostPop.AutoSize = true;
            this.lblHostPop.Location = new System.Drawing.Point(6, 25);
            this.lblHostPop.Name = "lblHostPop";
            this.lblHostPop.Size = new System.Drawing.Size(55, 13);
            this.lblHostPop.TabIndex = 24;
            this.lblHostPop.Text = "Hostname";
            // 
            // btnConnect
            // 
            this.btnConnect.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnConnect.Location = new System.Drawing.Point(171, 230);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(81, 23);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(14, 207);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 31;
            this.lblPassword.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword.Location = new System.Drawing.Point(73, 204);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(327, 20);
            this.txtPassword.TabIndex = 1;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(12, 181);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(55, 13);
            this.lblUsername.TabIndex = 29;
            this.lblUsername.Text = "Username";
            // 
            // lblPortPop
            // 
            this.lblPortPop.AutoSize = true;
            this.lblPortPop.Location = new System.Drawing.Point(35, 51);
            this.lblPortPop.Name = "lblPortPop";
            this.lblPortPop.Size = new System.Drawing.Size(26, 13);
            this.lblPortPop.TabIndex = 28;
            this.lblPortPop.Text = "Port";
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUsername.Location = new System.Drawing.Point(73, 178);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(327, 20);
            this.txtUsername.TabIndex = 0;
            // 
            // txtPortPop
            // 
            this.txtPortPop.Location = new System.Drawing.Point(67, 48);
            this.txtPortPop.Name = "txtPortPop";
            this.txtPortPop.Size = new System.Drawing.Size(60, 20);
            this.txtPortPop.TabIndex = 1;
            this.txtPortPop.Text = "110";
            // 
            // lstActivityPop
            // 
            this.lstActivityPop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstActivityPop.FormattingEnabled = true;
            this.lstActivityPop.HorizontalScrollbar = true;
            this.lstActivityPop.IntegralHeight = false;
            this.lstActivityPop.Location = new System.Drawing.Point(12, 273);
            this.lstActivityPop.Name = "lstActivityPop";
            this.lstActivityPop.Size = new System.Drawing.Size(388, 146);
            this.lstActivityPop.TabIndex = 3;
            // 
            // txtHostPop
            // 
            this.txtHostPop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHostPop.Location = new System.Drawing.Point(67, 22);
            this.txtHostPop.Name = "txtHostPop";
            this.txtHostPop.Size = new System.Drawing.Size(315, 20);
            this.txtHostPop.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtHostPop);
            this.groupBox1.Controls.Add(this.lblHostPop);
            this.groupBox1.Controls.Add(this.lblPortPop);
            this.groupBox1.Controls.Add(this.txtPortPop);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(388, 77);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Receiving - Pop3";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.chkAuth);
            this.groupBox2.Controls.Add(this.txtHostSmtp);
            this.groupBox2.Controls.Add(this.lblHostSmtp);
            this.groupBox2.Controls.Add(this.lblPortSmtp);
            this.groupBox2.Controls.Add(this.txtPortSmtp);
            this.groupBox2.Location = new System.Drawing.Point(12, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(388, 77);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sending - Smtp";
            // 
            // chkAuth
            // 
            this.chkAuth.AutoSize = true;
            this.chkAuth.Location = new System.Drawing.Point(134, 49);
            this.chkAuth.Name = "chkAuth";
            this.chkAuth.Size = new System.Drawing.Size(139, 17);
            this.chkAuth.TabIndex = 2;
            this.chkAuth.Text = "Requires Authentication";
            this.chkAuth.UseVisualStyleBackColor = true;
            // 
            // txtHostSmtp
            // 
            this.txtHostSmtp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHostSmtp.Location = new System.Drawing.Point(67, 22);
            this.txtHostSmtp.Name = "txtHostSmtp";
            this.txtHostSmtp.Size = new System.Drawing.Size(315, 20);
            this.txtHostSmtp.TabIndex = 0;
            // 
            // lblHostSmtp
            // 
            this.lblHostSmtp.AutoSize = true;
            this.lblHostSmtp.Location = new System.Drawing.Point(6, 25);
            this.lblHostSmtp.Name = "lblHostSmtp";
            this.lblHostSmtp.Size = new System.Drawing.Size(55, 13);
            this.lblHostSmtp.TabIndex = 24;
            this.lblHostSmtp.Text = "Hostname";
            // 
            // lblPortSmtp
            // 
            this.lblPortSmtp.AutoSize = true;
            this.lblPortSmtp.Location = new System.Drawing.Point(35, 51);
            this.lblPortSmtp.Name = "lblPortSmtp";
            this.lblPortSmtp.Size = new System.Drawing.Size(26, 13);
            this.lblPortSmtp.TabIndex = 28;
            this.lblPortSmtp.Text = "Port";
            // 
            // txtPortSmtp
            // 
            this.txtPortSmtp.Location = new System.Drawing.Point(67, 48);
            this.txtPortSmtp.Name = "txtPortSmtp";
            this.txtPortSmtp.Size = new System.Drawing.Size(60, 20);
            this.txtPortSmtp.TabIndex = 1;
            this.txtPortSmtp.Text = "25";
            // 
            // lstActivitySmtp
            // 
            this.lstActivitySmtp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstActivitySmtp.FormattingEnabled = true;
            this.lstActivitySmtp.HorizontalScrollbar = true;
            this.lstActivitySmtp.IntegralHeight = false;
            this.lstActivitySmtp.Location = new System.Drawing.Point(12, 438);
            this.lstActivitySmtp.Name = "lstActivitySmtp";
            this.lstActivitySmtp.Size = new System.Drawing.Size(388, 134);
            this.lstActivitySmtp.TabIndex = 4;
            // 
            // lblActivityPop
            // 
            this.lblActivityPop.AutoSize = true;
            this.lblActivityPop.Location = new System.Drawing.Point(11, 257);
            this.lblActivityPop.Name = "lblActivityPop";
            this.lblActivityPop.Size = new System.Drawing.Size(55, 13);
            this.lblActivityPop.TabIndex = 38;
            this.lblActivityPop.Text = "Receiving";
            // 
            // lblActivitySmtp
            // 
            this.lblActivitySmtp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblActivitySmtp.AutoSize = true;
            this.lblActivitySmtp.Location = new System.Drawing.Point(12, 422);
            this.lblActivitySmtp.Name = "lblActivitySmtp";
            this.lblActivitySmtp.Size = new System.Drawing.Size(46, 13);
            this.lblActivitySmtp.TabIndex = 39;
            this.lblActivitySmtp.Text = "Sending";
            // 
            // btnEmailError
            // 
            this.btnEmailError.Location = new System.Drawing.Point(12, 578);
            this.btnEmailError.Name = "btnEmailError";
            this.btnEmailError.Size = new System.Drawing.Size(75, 23);
            this.btnEmailError.TabIndex = 5;
            this.btnEmailError.Text = "Email Report";
            this.btnEmailError.UseVisualStyleBackColor = true;
            this.btnEmailError.Click += new System.EventHandler(this.btnEmailError_Click);
            // 
            // popWorkerThread
            // 
            this.popWorkerThread.WorkerReportsProgress = true;
            this.popWorkerThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.popWorkerThread_DoWork);
            this.popWorkerThread.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.popWorkerThread_ProgressChanged);
            this.popWorkerThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.popWorkerThread_RunWorkerCompleted);
            // 
            // smtpWorkerThread
            // 
            this.smtpWorkerThread.WorkerReportsProgress = true;
            this.smtpWorkerThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.smtpWorkerThread_DoWork);
            this.smtpWorkerThread.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.smtpWorkerThread_ProgressChanged);
            this.smtpWorkerThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.smtpWorkerThread_RunWorkerCompleted);
            // 
            // EmailChecker
            // 
            this.AcceptButton = this.btnConnect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 609);
            this.Controls.Add(this.btnEmailError);
            this.Controls.Add(this.lblActivitySmtp);
            this.Controls.Add(this.lblActivityPop);
            this.Controls.Add(this.lstActivitySmtp);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lstActivityPop);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.txtUsername);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "EmailChecker";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Email Checker ";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHostPop;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPortPop;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPortPop;
        private System.Windows.Forms.ListBox lstActivityPop;
        private System.Windows.Forms.TextBox txtHostPop;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtHostSmtp;
        private System.Windows.Forms.Label lblHostSmtp;
        private System.Windows.Forms.Label lblPortSmtp;
        private System.Windows.Forms.TextBox txtPortSmtp;
        private System.Windows.Forms.CheckBox chkAuth;
        private System.Windows.Forms.ListBox lstActivitySmtp;
        private System.Windows.Forms.Label lblActivityPop;
        private System.Windows.Forms.Label lblActivitySmtp;
        private System.Windows.Forms.Button btnEmailError;
        private System.ComponentModel.BackgroundWorker popWorkerThread;
        private System.ComponentModel.BackgroundWorker smtpWorkerThread;
    }
}

