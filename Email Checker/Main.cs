﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;
using System.ComponentModel;

namespace EmailChecker
{
    public partial class EmailChecker : Form
    {
        public EmailChecker()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            lstActivityPop.Items.Clear();
            lstActivitySmtp.Items.Clear();

            if (!string.IsNullOrEmpty(txtHostPop.Text))
            {
                if (string.IsNullOrEmpty(txtHostPop.Text) ||
                    string.IsNullOrEmpty(txtPortPop.Text) ||
                    string.IsNullOrEmpty(txtUsername.Text) ||
                    string.IsNullOrEmpty(txtPassword.Text))
                {
                    lstActivityPop.Items.Add("Missing Login Details");
                    lstActivityPop.BackColor = Color.Red;
                }
                else
                {
                    btnConnect.Enabled = false;
                    btnEmailError.Enabled = false;

                    popWorkerThread.RunWorkerAsync();
                }
            }

            if (!string.IsNullOrEmpty(txtHostSmtp.Text))
            {
                if (string.IsNullOrEmpty(txtHostSmtp.Text) ||
                                string.IsNullOrEmpty(txtPortSmtp.Text) ||
                                string.IsNullOrEmpty(txtUsername.Text) ||
                                string.IsNullOrEmpty(txtPassword.Text))
                {
                    lstActivitySmtp.Items.Add("Missing Login Details");
                    lstActivitySmtp.BackColor = Color.Red;
                }
                else
                {
                    btnConnect.Enabled = false;
                    btnEmailError.Enabled = false;

                    smtpWorkerThread.RunWorkerAsync();
                }
            }
        }

        // THREAD STUFF

        class StatusData
        {
            public string[] Items { get; set; }
            public Color BackColor { get; set; }
            public bool Clear { get; set; }
        }

        //POP3

        private void popWorkerThread_DoWork(object sender, DoWorkEventArgs e)
        {
            connectPop();
        }

        private void popWorkerThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!popWorkerThread.IsBusy && !smtpWorkerThread.IsBusy)
            {
                btnConnect.Enabled = true;
                btnEmailError.Enabled = true;
            }
        }

        private void popWorkerThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            StatusData status = e.UserState as StatusData;
            if (status == null)
                return;

            lstActivityPop.BackColor = status.BackColor;

            foreach (string st in status.Items)
            {
                lstActivityPop.Items.Add(st);
            }

            if (status.Clear)
                lstActivityPop.Items.Clear();
        }

        private void UpdatePopActivity(string items, Color backcolor, bool clear)
        {
            UpdatePopActivity(new string[] {items}, backcolor, clear);
        }

        private void UpdatePopActivity(string[] items, Color backcolor, bool clear)
        {
            popWorkerThread.ReportProgress(0, new StatusData() {
                Items = items,
                BackColor = backcolor,
                Clear = clear,
            });
        }

        // SMTP

        private void smtpWorkerThread_DoWork(object sender, DoWorkEventArgs e)
        {
            connectSmtp();
        }

        private void smtpWorkerThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!popWorkerThread.IsBusy && !smtpWorkerThread.IsBusy)
            {
                btnConnect.Enabled = true;
                btnEmailError.Enabled = true;
            }
        }

        private void smtpWorkerThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            StatusData status = e.UserState as StatusData;
            if (status == null)
                return;

            lstActivitySmtp.BackColor = status.BackColor;

            foreach (string st in status.Items)
            {
                lstActivitySmtp.Items.Add(st);
            }

            if (status.Clear)
                lstActivitySmtp.Items.Clear();
        }

        private void UpdateSmtpActivity(string items, Color backcolor, bool clear)
        {
            UpdateSmtpActivity(new string[] {items}, backcolor, clear);
        }

        private void UpdateSmtpActivity(string[] items, Color backcolor, bool clear)
        {
            smtpWorkerThread.ReportProgress(0, new StatusData()
            {
                Items = items,
                BackColor = backcolor,
                Clear = clear,
            });
        }

        // THREAD STUFF

        private void connectPop()
        {
            using (TcpClient popClient = new TcpClient())
            {
                try
                {
                    UpdatePopActivity("Connecting...", Color.Empty, false);
                    popClient.Connect(txtHostPop.Text, Convert.ToInt32(txtPortPop.Text));
                }
                catch (Exception err)
                {
                    UpdatePopActivity(new string[] {err.Message,"Disconnecting..."}, Color.Red, false);
                    return;
                }

                using (NetworkStream popStream = popClient.GetStream())
                using (StreamReader popReader = new StreamReader(popStream))
                using (StreamWriter popWriter = new StreamWriter(popStream))
                {
                    try
                    {
                        if (CheckErrPop(popReader.ReadLine(), popWriter))
                            return;

                        // Send Username
                        UpdatePopActivity("Sending USER", Color.Empty, false);
                        popWriter.WriteLine("USER " + txtUsername.Text);
                        popWriter.Flush();

                        if (CheckErrPop(popReader.ReadLine(), popWriter))
                            return;

                        // Send Password
                        UpdatePopActivity("Sending PASS", Color.Empty, false);
                        popWriter.WriteLine("PASS " + txtPassword.Text);
                        popWriter.Flush();

                        if (CheckErrPop(popReader.ReadLine(), popWriter))
                            return;

                        // Disconnect when successful
                        UpdatePopActivity("Disconnecting...", Color.Empty, false);
                        popWriter.WriteLine("QUIT");
                        popWriter.Flush();

                        if (CheckErrPop(popReader.ReadLine(), popWriter))
                            return;

                    }
                    catch
                    {
                        UpdatePopActivity("Lost Connection or timed Out", Color.Red, false);
                    }
                }
            }
        }

        private void connectSmtp()
        {
            using (TcpClient smtpClient = new TcpClient())
            {
                try
                {
                    UpdateSmtpActivity("Connecting...", Color.Empty, false);
                    smtpClient.Connect(txtHostSmtp.Text, Convert.ToInt32(txtPortSmtp.Text));
                }
                catch (Exception err)
                {
                    UpdateSmtpActivity(new string[] { err.Message, "Disconnecting..."}, Color.Red, false);
                    return;
                }
                using (NetworkStream smtpStream = smtpClient.GetStream())
                using (StreamReader smtpReader = new StreamReader(smtpStream))
                using (StreamWriter smtpWriter = new StreamWriter(smtpStream))
                {
                    try
                    {
                        if (CheckErrSmtp(smtpReader.ReadLine(), smtpWriter))
                            return;

                        if (chkAuth.Checked)
                        {
                            // Sending AUTH
                            UpdateSmtpActivity("Sending AUTH", Color.Empty, false);
                            smtpWriter.WriteLine("AUTH LOGIN");
                            smtpWriter.Flush();

                            if (CheckErrSmtp(smtpReader.ReadLine(), smtpWriter))
                                return;

                            // Sending USERNAME
                            UpdateSmtpActivity("Sending USER", Color.Empty, false);
                            smtpWriter.WriteLine(EncodeTo64(txtUsername.Text));
                            smtpWriter.Flush();

                            if (CheckErrSmtp(smtpReader.ReadLine(), smtpWriter))
                                return;

                            // Sending PASSWORD
                            UpdateSmtpActivity("Sending PASS", Color.Empty, false);
                            smtpWriter.WriteLine(EncodeTo64(txtPassword.Text));
                            smtpWriter.Flush();

                            if (CheckErrSmtp(smtpReader.ReadLine(), smtpWriter))
                                return;

                        }
                        UpdateSmtpActivity("Disconnecting...", Color.Empty, false);
                        smtpWriter.WriteLine("QUIT");
                        smtpWriter.Flush();

                        if (CheckErrSmtp(smtpReader.ReadLine(), smtpWriter))
                            return;
                    }
                    catch
                    {
                        UpdateSmtpActivity("Lost Connection or timed Out", Color.Red, false);
                    }
                }
            }
        }

        static public string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes
                  = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue
                  = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }


        private bool CheckErrPop(string errorIn, StreamWriter writerIn)
        {
            if (string.IsNullOrEmpty(errorIn))
                return false;

            if (errorIn.StartsWith("-") || errorIn.StartsWith("5") || !errorIn.StartsWith("+"))
            {
                UpdatePopActivity(new string[] {errorIn,"Disconnecting..."}, Color.Red, false);
                writerIn.WriteLine("QUIT");
                writerIn.Flush();
                return true;
            }
            else
            {
                UpdatePopActivity(errorIn, Color.LightGreen, false);
                return false;
            }
        }

        private bool CheckErrSmtp(string errorIn, StreamWriter writerIn)
        {
            if (string.IsNullOrEmpty(errorIn))
                return false;

            if (errorIn.StartsWith("-") || errorIn.StartsWith("5") || errorIn.StartsWith("+"))
            {
                UpdateSmtpActivity(new string[] {errorIn,"Disconnecting..."}, Color.Red, false);
                writerIn.WriteLine("QUIT");
                writerIn.Flush();
                return true;
            }
            else
            {
                UpdateSmtpActivity(errorIn, Color.LightGreen, false);
                return false;
            }
        }

        private void btnEmailError_Click(object sender, EventArgs e)
        {
            string newL = "%0A";
            string datePatt = @"M/d/yyyy hh:mm:ss tt";
            DateTime time1 = DateTime.UtcNow;
            string dtString = time1.ToString(datePatt);

            string authentication;
            if (chkAuth.Checked)
                authentication = "Yes";
            else
                authentication = "No";


            StringBuilder popError = new StringBuilder();

            if (lstActivityPop.Items.Count > 0)
            {
                foreach (string l in lstActivityPop.Items)
                {
                    popError.Append(l + newL);
                }
            }
            else
            {
                popError.Append("Pop Log Empty" + newL);
            }

            StringBuilder smtpError = new StringBuilder();

            if (lstActivitySmtp.Items.Count > 0)
            {
                foreach (string l in lstActivitySmtp.Items)
                {
                    smtpError.Append(l + newL);
                }
            }
            else
            {
                smtpError.Append("Smtp Log Empty" + newL);
            }

            System.Diagnostics.Process.Start("mailto:"
                + "?subject="
                + "Email tool report "
                + "&body="
                + "Date: " + dtString
                + newL
                + "Username: " + txtUsername.Text
                + newL
                + "Password: " + txtPassword.Text
                + newL
                + newL
                + "Pop Server: " + txtHostPop.Text
                + newL
                + "Port: " + txtPortPop.Text
                + newL
                + newL
                + "Error: "
                + newL
                + popError
                + newL
                + "Smtp Server: " + txtHostSmtp.Text
                + newL
                + "Port: " + txtPortSmtp.Text
                + newL
                + "Authentication: " + authentication
                + newL
                + newL
                + "Error: "
                + newL
                + smtpError
                    );
        }
    }
}
